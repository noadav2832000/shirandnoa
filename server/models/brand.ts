import mongoose from "mongoose";

interface IBrand {
  name: string
}

const brandSchema = new mongoose.Schema({
    name: {type: String, require: true}
  });

const Brand = mongoose.model("Brand", brandSchema)
export {Brand}