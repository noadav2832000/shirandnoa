import mongoose from "mongoose";

interface ICategory {
  name: string
}

const categorySchema = new mongoose.Schema({
    name: {type: String, require: true}
  });

const Category = mongoose.model("Category", categorySchema)
export {Category}