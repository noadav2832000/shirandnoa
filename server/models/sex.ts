import mongoose from "mongoose";

interface ISex {
  name: string
}

const sexSchema = new mongoose.Schema({
    name: String
  });

const Sex = mongoose.model("Sex", sexSchema)
export {Sex}