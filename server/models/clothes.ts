import { ObjectId } from "mongodb";
import mongoose from "mongoose";
import { Schema } from "mongoose";

interface IClothes { 
  title: string,
  description: string, 
  sex: ObjectId,
  category: ObjectId,
  brand: ObjectId,
  imgSrc: string
}

const clothesSchema = new mongoose.Schema({
    title:  String,
    sex: { type: Schema.Types.ObjectId, ref: 'Sex' },
    brand: { type: Schema.Types.ObjectId, ref: 'Brand' },
    category: { type: Schema.Types.ObjectId, ref: 'Category' },
    description: String,
    imgSrc: String
  });

 const Clothes = mongoose.model("Clothes", clothesSchema)
 export {Clothes}