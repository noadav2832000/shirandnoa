import express from "express"
var router = express.Router()
import { Request, Response } from 'express';
import mongoose from 'mongoose';
import { Category } from "../models/category";

const createCategory = (req: Request, res: Response) => {
    const { name } = req.body;

    const category = new Category({
        _id: new mongoose.Types.ObjectId(),
        name
    });

    return category
        .save()
        .then((category) => res.status(201).json({ category }))
        .catch((error) => res.status(500).json({ error }));
};

const getCategory = (req: Request, res: Response) => {
    const categoryId = req.params.categoryId;

    return Category.findById(categoryId)
        .then((category) => (category ? res.status(200).json({ category }) : res.status(404).json({ message: 'not found' })))
        .catch((error) => res.status(500).json({ error }));
};

const getAllCategories = (req: Request, res: Response) => {
    return Category.find()
        .then((categories) => res.status(200).json({ categories }))
        .catch((error) => res.status(500).json({ error }));
};

const updateCategory = (req: Request, res: Response) => {
    const categoryId = req.params.categoryId;

    return Category.findById(categoryId)
        .then((category) => {
            if (category) {
                category.set(req.body);

                return category
                    .save()
                    .then((category) => res.status(201).json({ category }))
                    .catch((error) => res.status(500).json({ error }));
            } else {
                return res.status(404).json({ message: 'not found' });
            }
        })
        .catch((error) => res.status(500).json({ error }));
};

const deleteCategory = (req: Request, res: Response) => {
    const categoryId = req.params.categoryId;

    return Category.findByIdAndDelete(categoryId)
        .then((category) => (category ? res.status(201).json({ category, message: 'Deleted' }) : res.status(404).json({ message: 'not found' })))
        .catch((error) => res.status(500).json({ error }));
};

export default { createCategory, getCategory, getAllCategories, updateCategory, deleteCategory };

export {router as categoriesRouter}