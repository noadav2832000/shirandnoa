import express from "express"
var router = express.Router()
import { Request, Response } from 'express';
import mongoose from 'mongoose';
import { Clothes } from "../models/clothes";

const createCloth = (req: Request, res: Response) => {
    const { title,
        description, 
        sex,
        category,
        brand,
        imgSrc} = req.body;

    const cloth = new Clothes({
        _id: new mongoose.Types.ObjectId(),
        title,
        description, 
        sex,
        category,
        brand,
        imgSrc
    });

    return cloth
        .save()
        .then((cloth) => res.status(201).json({ cloth }))
        .catch((error) => res.status(500).json({ error }));
};

const getCloth = (req: Request, res: Response) => {
    const clothId = req.params.clothId;

    return Clothes.findById(clothId)
        .then((cloth) => (cloth ? res.status(200).json({ cloth }) : res.status(404).json({ message: 'not found' })))
        .catch((error) => res.status(500).json({ error }));
};

const getAllClothes = (req: Request, res: Response) => {
    return Clothes.find()
        .then((clothes) => res.status(200).json({ clothes }))
        .catch((error) => res.status(500).json({ error }));
};

const updateCloth = (req: Request, res: Response) => {
    const clothId = req.params.clothId;

    return Clothes.findById(clothId)
        .then((cloth) => {
            if (cloth) {
                cloth.set(req.body);

                return cloth
                    .save()
                    .then((cloth) => res.status(201).json({ cloth }))
                    .catch((error) => res.status(500).json({ error }));
            } else {
                return res.status(404).json({ message: 'not found' });
            }
        })
        .catch((error) => res.status(500).json({ error }));
};

const deleteCloth = (req: Request, res: Response) => {
    const clothId = req.params.clothId;

    return Clothes.findByIdAndDelete(clothId)
        .then((cloth) => (cloth ? res.status(201).json({ cloth, message: 'Deleted' }) : res.status(404).json({ message: 'not found' })))
        .catch((error) => res.status(500).json({ error }));
};

export default { createCloth, getCloth, getAllClothes, updateCloth, deleteCloth };

export {router as clothesRouter}