import express, {NextFunction, Request, Response} from "express"
import mongoose from "mongoose"
var router = express.Router()
import {Brand} from "../models/brand"

const createBrand = (req: Request, res: Response, next: NextFunction) => {
    const { name } = req.body

    const brand = new Brand({
        _id: new mongoose.Types.ObjectId(),
        name
    })

    return brand.save()
    .then((brand) => res.status(201).json({ brand }))
    .catch(error => res.status(500).json({ error }));
}

const getAllBrands = (req: Request, res: Response, next: NextFunction) => {
    return Brand.find()
    .then((brands) => res.status(201).json({ brands }))
    .catch(error => res.status(500).json({ error }));
}

const getBrand = (req: Request, res: Response) => {
    const brandId = req.params.brandId;

    return Brand.findById(brandId)
        .then((brand) => (brand ? res.status(200).json({ brand }) : res.status(404).json({ message: 'not found' })))
        .catch((error) => res.status(500).json({ error }));
};

const updateBrand = (req: Request, res: Response) => {
    const brandId = req.params.brandId;

    return Brand.findById(brandId)
        .then((brand) => {
            if (brand) {
                brand.set(req.body);

                return brand
                    .save()
                    .then((brand) => res.status(201).json({ brand }))
                    .catch((error) => res.status(500).json({ error }));
            } else {
                return res.status(404).json({ message: 'not found' });
            }
        })
        .catch((error) => res.status(500).json({ error }));
};

const deleteBrand = (req: Request, res: Response) => {
    const brandId = req.params.brandId;

    return Brand.findByIdAndDelete(brandId)
        .then((brand) => (brand ? res.status(201).json({ brand, message: brandId + ' Deleted' }) : res.status(404).json({ message: 'brandId is not found' })))
        .catch((error) => res.status(500).json({ error }));
};
export default { createBrand, getAllBrands, getBrand, updateBrand, deleteBrand}