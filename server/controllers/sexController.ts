import express, {Request, Response} from "express"
import { Sex } from "../models/sex";
var router = express.Router()

const getAllSexes = (req: Request, res: Response) => {
    return Sex.find()
    .then((sexes) => res.status(201).json({ sexes }))
    .catch(error => res.status(500).json({ error }));
}

export default { getAllSexes}

export {router as sexesRouter}