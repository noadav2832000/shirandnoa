import express from "express"
import { json } from "body-parser";
import { clothesRouter } from "./controllers/clothesController";
import mongoose from "mongoose";
import categoriesRoutes from "./routes/Category";
import clothesRoutes from "./routes/Clothes";
import brandRoutes from './routes/Brand'
import sexRoutes from './routes/Sex'
import bodyParser from 'body-parser';

const PORT = process.env.PORT || 3001;

const app = express();
var cors = require('cors');
app.use(cors());
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}))
app.use('/brands', brandRoutes)
app.use('/categories', categoriesRoutes)
app.use('/clothes', clothesRoutes)
app.use('/sexes', sexRoutes)

mongoose.connect('mongodb://localhost:27017/m2mClothes',
  err => {
    if (!err) {
      console.log("Connected");
    } else {
      console.log("Error" + err)
    }
});

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));

db.once('open', function(callback) {
    console.log("connection to db open")
});

app.listen(PORT, () => {
    console.log(`Server listening on ${PORT}`);
});