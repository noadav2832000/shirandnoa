import express from 'express'
import brandController from '../controllers/brandController'

const router = express.Router();

router.get('', brandController.getAllBrands)
router.post('', brandController.createBrand)
router.get('/:brandId', brandController.getBrand);
router.patch('/:brandId', brandController.updateBrand);
router.delete('/:brandId', brandController.deleteBrand);
export = router