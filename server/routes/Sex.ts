import express from 'express';
import sexController from '../controllers/sexController';

const router = express.Router();

router.get('/', sexController.getAllSexes);

export = router;