import express from 'express';
import clothesController from '../controllers/clothesController';

const router = express.Router();

router.get('/', clothesController.getAllClothes);
router.post('/', clothesController.createCloth);
router.get('/:clothId', clothesController.getCloth);
router.patch('/:clothId', clothesController.updateCloth);
router.delete('/:clothId', clothesController.deleteCloth);

export = router;