import React from "react";
import "./CatalogCard.css";
import PropTypes, { InferProps } from 'prop-types';

const CatalogCard = (props : InferProps<typeof CatalogCard.propTypes>) => {
    return (
        // <div>
        <div>
            <div className={"catalog-card-container"}>
                <img className={"big-img"} src={props.imagesList[0] ?? ""} alt=''/>
                <img className={"small-img-first"} src={props.imagesList[1] ?? ""} alt=''/>
                <img className={"small-img-second"} src={props.imagesList[2] ?? ""} alt=''/>

                {/*<img className={"small-img-first"} src={"../../images/ab_tiger_sweatshirt.webp"} alt={""}/>*/}
                {/*<img className={"small-img-second"} src={"../../images/ab_tiger_sweatshirt.webp"}/>*/}
            </div>
            </div>
    );
}

CatalogCard.prototype = {
    imagesList: PropTypes.array
};

export default CatalogCard;