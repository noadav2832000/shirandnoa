import React, {useState} from "react";
import "./Search.css";
import {BiSearch} from "react-icons/bi";

const Search = () => {
    const [searchText, setSearchText] = useState<string>("");
    
    // TODO: change "any" to the correct type
    const handleInputChange: Function = (event: any): void => {
        setSearchText(event.target.value);
    }

    const onKeyPress: Function = (event: KeyboardEvent): void => {
        if (event.key === 'Enter') {
            doSearch()
          }
    }

    const doSearch: Function = (event: Event): void => {            
        const newUrl = `/searchedClothes/${searchText}`;
        window.location.href = newUrl;
    }

    // componentWillUnmount
    // reset searched items in the store    

    // TODO: change the way of calling functions
    return (
        <div className="input-group">
            <input className="form-control border-end-0 border" type="search" placeholder="search for..." 
                    onChange={() =>handleInputChange()} onKeyPress={() => onKeyPress()}/>
            <span className="input-group-append border btn">
                <BiSearch onClick={() => doSearch()}></BiSearch>
            </span>
        </div>
    );   
}

export default Search;