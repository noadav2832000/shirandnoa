import React from "react";
import "./OpenMenu.css";
import {BiX} from "react-icons/bi";
import {Link} from "react-router-dom";
import PropTypes, { InferProps } from 'prop-types';

const OpenMenu = (props: InferProps<typeof OpenMenu.propTypes>) => {
    function getMenuListHtml() {
      const menuOptionsList: Array<object> = [
        { name: "MAIN CATALOG", linkTo: "/" },
        { name: "MAN", linkTo: "/man" },
        { name: "WOMAN", linkTo: "/woman" },
        { name: "ABOUT US", linkTo: "/about" },
        { name: "CONTACT", linkTo: "/contact" },
      ];

      let menuOptionsListHtml: Array<React.ReactElement> = [];
      menuOptionsList.forEach((currMenuOption: any) => {
        menuOptionsListHtml.push(
          <Link
            key={currMenuOption.name}
            className={"menu-option"}
            to={currMenuOption.linkTo}
            onClick={props.closeMenuFunc}
          >
            {currMenuOption.name}
          </Link>
        );
        menuOptionsListHtml.push(<br key={currMenuOption.name + 1} />);
      });
      return menuOptionsListHtml;
    }

    return (
        <div className={"menu-container"}>
            <BiX className={"exit-btn"} onClick={props.closeMenuFunc}/>
            <div className={"menu-options-list"}>
                {getMenuListHtml()}
            </div>
        </div>
    );
}

OpenMenu.prototype = {
    closeMenuFunc: PropTypes.func
}
export default OpenMenu;