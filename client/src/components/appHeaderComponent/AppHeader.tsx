import React, {useState} from "react";
import "./AppHeader.css";
import {BiMenu, BiSearch, BiShoppingBag} from "react-icons/bi";
import logo from "../../images/m2m-logo.svg";
import OpenMenu from "../menuComponent/OpenMenu";
import Search from "../searchComponent/Search";
import PropTypes, { InferProps } from 'prop-types';

const AppHeader = () => {
    const [isMenuBtnOn, setIsMenuBtnOn] = useState<Boolean>(false);
    const [isSearchBtnOn, setIsSearchBtnOn] = useState<Boolean>(false);

    return (
    <div>
        <FirstLineHeader></FirstLineHeader>
        <div className="App-header-container">
            <div className={"second-line-header-container"}>
                <Menu isMenuBtnOn={isMenuBtnOn} onMenuClick={() => setIsMenuBtnOn(!isMenuBtnOn)}></Menu>
                <AppLogo></AppLogo>
                <div className={"right-section"}>
                    <BiSearch className={"search-btn"} onClick={() => setIsSearchBtnOn(!isSearchBtnOn)}/>
                    <BiShoppingBag className={"bag-btn"}/>                    
                </div>
            </div>
            {isSearchBtnOn && <Search></Search>}
        </div>
    </div>
        );
    
}

const FirstLineHeader = () => {
    return(
        <div className={"first-line-header-container"}>
                {/*<span>AD catalog</span>*/}
            </div>
    );
}

const Menu = (props: InferProps<typeof Menu.propTypes>) => {
    return (
        <div>
            <div className={"menu-btn"} onClick={props.onMenuClick}>
                <BiMenu/>
                <span className={"menu-font"}> MENU</span>
            </div>
            {props.isMenuBtnOn &&
                <OpenMenu closeMenuFunc={props.onMenuClick}/>}
        </div>

    )
}

Menu.prototype = {
    onMenuClick: PropTypes.array
}

const AppLogo = () => {
    return(
        <div className={"app-logo-container"}>
            <img src={logo} className="App-logo" alt="logo"/>
            <span className="App-logo-span">M 2 M</span>
        </div>
    );
}

export default AppHeader;