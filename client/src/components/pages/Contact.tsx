import React from "react";
import "./Contact.css";
import {FaWaze} from "react-icons/fa";
import {BsWhatsapp} from "react-icons/bs";
import AppHeader from "../appHeaderComponent/AppHeader";

const Contact = () => {
    const shopAdress = {
        linkLat: 32.1930056648843,
        linkLng: 34.82017993927003    
    }
    const amitsNumber = "0504557888";
    
    return (
      <div>
        <AppHeader />
        <div className="contact-container">
          <img
            src={require("../../images/find_your_own_style.webp")}
            width="50%"
            alt=""
          />
          <div className="contact-text-line"> טלפון: 0504557888</div>
          <div className="contact-text-line">
            {" "}
            כתובת: האורנים 22, כפר שמריהו
          </div>
          <div className="contact-text-line"> שעות פעילות: 12:00-22:00</div>
          <div className="btn-container">
            <a
              className="waze-navigate-btn"
              href={`https://waze.com/ul?ll=${shopAdress.linkLat},${shopAdress.linkLng}&navigate=yes`}
            >
              <FaWaze></FaWaze>
              {"  פתח מיקום בווייז"}
            </a>
          </div>

          <div className="btn-container">
            <a
              className="open-whatsapp-btn"
              href={`http://api.whatsapp.com/send?phone=972${amitsNumber}`}
            >
              <BsWhatsapp></BsWhatsapp>
              {"   שלח הודעת ווטסאפ"}
            </a>
          </div>
        </div>
      </div>
    );
}

export default Contact;