import React, { ReducerAction, useEffect, useState } from "react";
import { clothesData } from "../data/clothesData";
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import Clothes from "../../models/clothes";
import AppHeader from "../appHeaderComponent/AppHeader";

const SearchedClothes = () => {
    const [filteredClothesHtml, setFilteredClothesHtml] = useState<Array<React.ReactElement>>([]);

    useEffect(() => {
        setFilteredClothesHtml(buildFilteredClothesHtml());
    }, []);
    
    const buildFilteredClothesHtml: Function = () => {            
            const searchText: string = window.location.href.split('/')[4];
            const allClothes: Array<any> = clothesData.womanCatalog.concat(clothesData.manCatalog);
            const filteredClothes: Array<Clothes> = allClothes.filter((item) => 
                    item.description?.toLowerCase().includes(searchText.toLowerCase()) || 
                    item.brand?.toLowerCase().includes(searchText.toLowerCase()) ||
                    item.title?.toLowerCase().includes(searchText.toLowerCase()));
            const filteredClothesHtml: Array<React.ReactElement> = filteredClothes.map((item: Clothes, index) => {            
                return (
                  <div key={index}>
                    <Card
                      className="item-card"
                      id={item.id}
                      style={{ width: "18rem" }}
                    >
                      <Card.Img variant="top" src={item.imgSrc} />
                      <Card.Body>
                        <Card.Title>{item.title ?? ""}</Card.Title>
                        <Card.Text>{item.description ?? ""}</Card.Text>
                        <Button
                          style={{
                            backgroundColor: "rgb(161, 138, 113)",
                            border: "none",
                          }}
                        >
                          הוסף לסל
                        </Button>
                      </Card.Body>
                    </Card>
                    {index % 4 === 0 && <br />}
                  </div>
                  // <div className="card-container" key={index}>
                  //     <img className="card-img" src={item.imgSrc} alt=''/>
                  //     <div className="card-text">
                  //         <span className="card-title">{item.title ?? ""}</span>
                  //         <span className="card-description">{item.description ?? ""}</span>
                  //     </div>
                  //     <Button className="add-to-cart-btn">הוסף לסל</Button>
                  // </div>
                );
            });
            return filteredClothesHtml;
    }   
    
    return (
      <div>
        <AppHeader />
        <div className="searched-clothes-container">{filteredClothesHtml}</div>
      </div>
    );
}

export default SearchedClothes;