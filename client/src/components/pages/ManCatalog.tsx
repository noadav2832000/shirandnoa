import React, {useEffect, useState} from "react";
import CatalogCard from "../cards/CatalogCard";
import {clothesData} from "../data/clothesData.js";
import AppHeader from "../appHeaderComponent/AppHeader";

const ManCatalog = () => {
    const [manCatalog, setManCatalog] = useState<Array<React.ReactElement>>([])

    // componentDidMount
    useEffect(() => {
        setManCatalog(generatemanCatalog());
      }, []);


    const generatemanCatalog: Function = () => {
        const catalogCards: Array<React.ReactElement> = [];
        const manCatalog = clothesData["manCatalog"];
        for (let index = 0; index < manCatalog.length; index++) {
            let imagesList: Array<string>;
            let item = manCatalog[index];
            if (manCatalog.length - 1 >= 3) {
                imagesList = [item.imgSrc, manCatalog[index + 1].imgSrc, manCatalog[index + 2].imgSrc];
                index = index + 3;
                catalogCards.push(<CatalogCard key={item.id} imagesList={imagesList}></CatalogCard>)
            } else {
                imagesList = [item.imgSrc];
                catalogCards.push(<img key={item.id} src={item.imgSrc} alt=''/>)

            }
            // catalogCards.push(<CatalogCard imagesList={imagesList}></CatalogCard>)
        }
    
        console.log(catalogCards);
        return catalogCards;
    };

    return (
        <div className={"home"}>
            <AppHeader></AppHeader>
            {/* <video className="content__video js-content-video hide-until@sm" loop="true" autoPlay="autoplay"
            muted="" aria-hidden="true" playsInline="" width="80%" height="700">
            <source src="https://cdn.shopify.com/videos/c/o/v/9b4bc4e1cf644159840314d1e76f947f.mp4"
                    type="video/mp4"/>
                </video> */}

            {manCatalog}

        </div>
    );
    
}

export default ManCatalog;