export const clothesData = {
    "womanCatalog": [
        {
            "id": 1,
            "title": "T-shirt",
            "brand": "Anine Bing",
            "imgSrc": require("../../images/ab_tiger_sweatshirt.webp")
        },
        {
            "id": 2,
            "title": "T-shirt",
            "brand": "Anine Bing",
            "imgSrc":  require("../../images/ab_tiger_sweatshirt.webp")
        },
        {
            "id": 3,
            "title": "T-shirt",
            "brand": "Anine Bing",
            "imgSrc":  require("../../images/ab_tiger_sweatshirt.webp")
        },
        {
            "id": 4,
            "title": "T-shirt",
            "brand": "Anine Bing",
            "imgSrc":  require("../../images/ab_tiger_sweatshirt.webp")
        }
    ],

    "manCatalog": [
        {
            "id": 1,
            "title": "T-shirt",
            "brand": "REPRESENT",
            "imgSrc": require("../../images/represent_grey_sweather.webp")
        },
        {
            "id": 2,
            "title": "T-shirt",
            "brand": "Anine Bing",
            "imgSrc": require("../../images/represent_grey_sweather.webp")
        },
        {
            "id": 3,
            "title": "T-shirt",
            "brand": "Anine Bing",
            "imgSrc": require("../../images/represent_grey_sweather.webp")
        },
        {
            "id": 4,
            "title": "T-shirt",
            "brand": "Anine Bing",
            "imgSrc": require("../../images/represent_grey_sweather.webp")
        },
        {
            "id": 5,
            "title": "T-shirt",
            "brand": "REPRESENT",
            "imgSrc": require("../../images/ab_tiger_sweatshirt.webp")
        },
        {
            "id": 6,
            "title": "T-shirt",
            "brand": "Anine Bing",
            "imgSrc": require("../../images/represent_grey_sweather.webp")
        },
        {
            "id": 7,
            "title": "T-shirt",
            "brand": "Anine Bing",
            "imgSrc": require("../../images/represent_grey_sweather.webp")
        },
        {
            "id": 8,
            "title": "T-shirt",
            "brand": "Anine Bing",
            "imgSrc": require("../../images/ab_tiger_sweatshirt.webp")
        }
    ]
}