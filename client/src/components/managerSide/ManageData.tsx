import * as React from 'react';
import { ControlersTypes } from './ManagerRequestsExucator';
import DataTable from './ManageTable';
import { BrandQueries, ClothesQueries, CategoryQueries } from './queries';

const ManageData = () => {
    const {data: brands, status: brandsStatus} = BrandQueries.useData();
    const {data: clothes, status: clothesStatus} = ClothesQueries.useData()
    const {data: category, status: categoryStatus} = CategoryQueries.useData();

    return (
      <>
        <div>
        <h2>Brands table</h2>
          {brandsStatus === "error" && <p>Error fetching brands data</p>}
          {brandsStatus === 'loading' && <p> loading brands... </p>}
          {brandsStatus === 'success' && brands && <DataTable dataList={brands.brands} 
            controllerType={ControlersTypes.Brands} queriesExecutor={BrandQueries}></DataTable>}
        </div>
        <div>
        <h2>Clothes table</h2>
          {clothesStatus === "error" && <p>Error fetching clothes data</p>}
          {clothesStatus === 'loading' && <p> loading clothes... </p>}
          {clothesStatus === 'success' && clothes && <DataTable dataList={clothes.clothes} 
            controllerType={ControlersTypes.Clothes} queriesExecutor={ClothesQueries}></DataTable>}
        </div>
        <div>
        <h2>Categories table</h2>
          {categoryStatus === "error" && <p>Error fetching categories data</p>}
          {categoryStatus === 'loading' && <p> loading categories... </p>}
          {categoryStatus === 'success' && category && <DataTable dataList={category.categories} 
            controllerType={ControlersTypes.Categories} queriesExecutor={CategoryQueries}></DataTable>}
        </div>
      </>
    );
}


export default ManageData;