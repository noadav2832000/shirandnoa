import { ControlersTypes } from "../ManagerRequestsExucator";
import * as React from 'react';
import {
  Button,
  useDisclosure,
  Input,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalCloseButton,
  ModalBody
} from '@chakra-ui/react';
import './DataTable.css'
import ClothesInputForm from "./ClothesInputForm";

interface IAddItemProps{
  modelType: ControlersTypes
  modelProps: Array<string>
  queriesExecutor: any
}

function AddItem({modelType, modelProps, queriesExecutor}: IAddItemProps) {
    const { isOpen, onOpen, onClose } = useDisclosure()  
    const {mutate: addItem} = queriesExecutor.useAdd()
    const initialObjectForNewData = {};
    modelProps.forEach((prop: string) => { 
        if (prop !== 'id') {initialObjectForNewData[prop] = ''}
    })
    const [newData, setNewData] = React.useState<object>(initialObjectForNewData);
    const handleDataChange = (newValue, propName: string) => { setNewData({...newData, [propName]: newValue}) };

    const formHtmlInputs = modelType === ControlersTypes.Clothes ? 
    <ClothesInputForm handleDataChange={handleDataChange}/> :
     Object.keys(initialObjectForNewData).map((property: string) =>
        <Input key={property} variant='flushed' placeholder={property + '...'} onChange={(event) => handleDataChange(event.target.value, property)}/>
    )

    const submitForm = () => {
      addItem(newData);
      onClose();
    }
  
    return (
      <>
        <Button onClick={onOpen}>POST</Button>
        <Modal isOpen={isOpen} onClose={onClose}>
          <ModalOverlay />
          <ModalContent>
            <ModalHeader>Create New {modelType}</ModalHeader>
            <ModalBody>{formHtmlInputs}</ModalBody>
            <ModalCloseButton />
            <ModalFooter>
              <Button colorScheme='blue' mr={3} onClick={submitForm}>
                Save
              </Button>
              <Button onClick={onClose}>Cancel</Button>
            </ModalFooter>
          </ModalContent>
        </Modal>
      </>
    )
  }

  export default AddItem