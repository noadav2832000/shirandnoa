import * as React from 'react';
import { IBrand, IClothes, ICategory, ISex } from '../../../interfaces/clothes';
import PropTypes, { InferProps } from 'prop-types';
import {
  Button,
  useDisclosure,
  Input,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalCloseButton,
  ModalBody,
  EditableInput,
} from '@chakra-ui/react';
import Select from 'react-select';
import ManagerRequestsExucator, { ControlersTypes } from '../ManagerRequestsExucator';
import { useQuery, useQueryClient } from "react-query";
import { useState } from 'react';
import './DataTable.css'
import ClothesInputForm from './ClothesInputForm';

interface IEditTable {
  modelType: ControlersTypes
  itemId: string
  queriesExecutor: any
}

function EditTable({queriesExecutor, itemId, modelType}: IEditTable) {
    const { isOpen, onOpen, onClose } = useDisclosure()  
    const [newData, setNewData] = React.useState({});
    const {mutate: editItem} = queriesExecutor.useUpdate();
    const handleDataChange = (newValue, propName: string) => { setNewData({...newData, [propName]: newValue}) };

    React.useEffect(() => {
      isOpen && queriesExecutor.getById(itemId).then((item) => {
        const currModel = modelType === ControlersTypes.Brands ? 'brand' : modelType === ControlersTypes.Categories ? 'category' : 'clothes';
        setNewData(item[currModel])
      });
    }, [isOpen])
        
  
    const formHtmlInputs = modelType === ControlersTypes.Clothes ?
     <ClothesInputForm handleDataChange={handleDataChange}/> :
     Object.keys(newData).map((property: string) =>
        <Input key={property} defaultValue={newData[property]} variant='flushed' 
        placeholder={property + '...'} onChange={(event) => handleDataChange(event, property)}/>
    )
  
    const submitForm = () => {
      editItem(itemId, newData);
      onClose();
    }
  
    return (
      <>
        <Button onClick={onOpen}>EDIT</Button>
        <Modal isOpen={isOpen} onClose={onClose}>
          <ModalOverlay />
          <ModalContent>
            <ModalHeader>Edit {modelType}</ModalHeader>
            {newData && <ModalBody>{formHtmlInputs}</ModalBody>}
            <ModalCloseButton />
            <ModalFooter>
              <Button colorScheme='blue' mr={3} onClick={submitForm}>
                Save
              </Button>
              <Button onClick={onClose}>Cancel</Button>
            </ModalFooter>
          </ModalContent>
        </Modal>
      </>
    )
  }

export default EditTable