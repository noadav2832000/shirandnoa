import * as React from 'react';
import {
  Table,
  Thead,
  Tbody,
  Tr,
  Th,
  Td,
  TableContainer,
  Button,
} from '@chakra-ui/react';
import { ControlersTypes } from '../ManagerRequestsExucator';
import { useState } from 'react';
import './DataTable.css'
import EditTable from './EditTable';
import AddItem from './AddItem';
import { useDelete } from '../queries/BrandsQueries';
import { BrandQueries, CategoryQueries, ClothesQueries } from '../queries';

interface IDataTableProps{
    controllerType: ControlersTypes
    dataList: Array<object>
    queriesExecutor: any
}

function DataTable({controllerType, dataList, queriesExecutor}: IDataTableProps) {
    const typeAndInterface = {
        [ControlersTypes.Brands]: ['id', 'name'],
        [ControlersTypes.Categories]: ['id', 'name'],
        [ControlersTypes.Clothes]: ["title", "description", "imgSrc", "brand", "sex", "category"],
    }     
     const {mutate: deleteItem} = queriesExecutor.useDelete()

    const modelProps = typeAndInterface[controllerType];
    const [markedItemId, setMarkedItemId] = useState();
    
    const rows = dataList.map((item: any, rowIndex: Number) => 
        <Tr key={item._id} onClick={() => {setMarkedItemId(item._id)}
          } className={item._id === markedItemId ? 'marked' : ''}>
            <Td>{item._id}</Td>
            <Td>{item.name}</Td>
            <EditTable modelType={controllerType} queriesExecutor={queriesExecutor} itemId={item._id}/>
        </Tr>
    );

    const headers = modelProps.map((key: string) => <Th key={key}>{controllerType + ' ' + key}</Th>);
   
    const onDeleteClick = () => {
        if (markedItemId) {
            deleteItem(markedItemId);
        }
    };
  
    return ( <div>
      <TableContainer marginBottom={"20vh"} >
      <Table variant='simple'>
        <Thead>
          <Tr>
            <Th><AddItem modelType={controllerType} queriesExecutor={queriesExecutor} modelProps={modelProps}/></Th>
            <Th><Button onClick={onDeleteClick}>Delete</Button></Th>
          </Tr>
          <Tr>
            {headers}
          </Tr>
        </Thead>
        <Tbody>
          {rows}
        </Tbody>
      </Table>
    </TableContainer></div>
    );
  }

  export default DataTable