import * as React from 'react';
import { IBrand, ICategory, ISex } from '../../../interfaces/clothes';
import {Input} from '@chakra-ui/react';
import Select from 'react-select';
import './DataTable.css'
import {BrandQueries, CategoryQueries, SexQueries}  from '../queries/';

interface IClothesInputForm{
    handleDataChange: (newValue: any, property: string) => void
}

function ClothesInputForm({handleDataChange}: IClothesInputForm) {
    const {data: sexesData} = SexQueries.useData();    
    const {data: brands} = BrandQueries.useData()
    const {data: categories} = CategoryQueries.useData()

    const brandsOptions = brands?.brands.map((currBrand: IBrand) => {return {value: currBrand._id, label: currBrand.name}} )
    const sexesOptions = sexesData?.sexes.map((currSex: ISex) => {return {value: currSex._id, label: currSex.name}} )
    const categoriesOptions = categories?.categories.map((currCategory: ICategory) => {return {value: currCategory._id, label: currCategory.name}} )
    
    return (
        <>
        <Input className='input-container' variant='flushed' placeholder={'title...'} onChange={(event) => handleDataChange(event.target.value, 'name')}/>
        <Input className='input-container' variant='flushed' placeholder={'description...'} onChange={(event) => handleDataChange(event.target.value, 'description')}/>
        <Input className='input-container' variant='flushed' placeholder={'imgSrc...'} onChange={(event) => handleDataChange(event.target.value, 'imgSrc')}/>
        
        <Select className='input-container' options={brandsOptions}  placeholder='brands...' onChange={(newVal) => handleDataChange(newVal, 'brand')}/>
        <Select className='input-container' options={sexesOptions}  placeholder='sexes...' onChange={(newVal) => handleDataChange(newVal, 'sex')}/>
        <Select className='input-container' options={categoriesOptions}  placeholder='categories...' onChange={(newVal) => handleDataChange(newVal, 'category')}/>        
        </>
    );
}

export default ClothesInputForm