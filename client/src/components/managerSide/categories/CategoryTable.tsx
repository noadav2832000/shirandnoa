import { Button, Input, Table, TableCaption, TableContainer, Tbody, Th, Td, Thead, Tr, useDisclosure } from '@chakra-ui/react';
import PropTypes, { InferProps } from 'prop-types';
import React, { useState } from "react";
import { ICategory } from '../../../interfaces/dataBase';
import {
    Modal,
    ModalOverlay,
    ModalContent,
    ModalHeader,
    ModalFooter,
    ModalCloseButton,
  } from '@chakra-ui/react'
import { useQuery } from 'react-query';
import './CategoryTable.css';

const CategoryTable = (props: InferProps<typeof CategoryTable.propTypes>) => {
    const [markedCategoryId, setMarkedCategoryId] = useState();
    const newMarkedCategory = (categoryId) => {
        console.log("NEW MARKED ROW")
        console.log(markedCategoryId)
        setMarkedCategoryId(categoryId)
    };

    const rows = props.categoriesList ? props.categoriesList.categories.map((category: ICategory, rowIndex: Number) => {
        return <Tr key={category._id} onClick={() => newMarkedCategory(category._id)} className={category._id === markedCategoryId ? 'marked' : ''}>
            <Td>{category._id}</Td><Td>{category.name}</Td></Tr>;
    }) : '';
    
    const onEditClick = () => {
        
    };
  
    const onDeleteClick = () => {
        if (markedCategoryId) {
        fetch("http://localhost:3001/categories/delete/" + markedCategoryId)
            .then(res => console.log(res))
        }
    };

    return(
        <div>
        <TableContainer marginLeft={"50vh"} marginTop={"20vh"} width="50%">
        <Table variant='simple'>
          <TableCaption>Imperial to metric conversion factors</TableCaption>
          <Thead>
            <Tr>
              <Th><CategoryFormModal/></Th>
              <Th><CategoryEditFormModal markedCategoryId={markedCategoryId}/></Th>
              <Th><div onClick={onDeleteClick}>Delete</div></Th>
            </Tr>
            <Tr>
              <Th>Category Id</Th>
              <Th>Category Name</Th>          
            </Tr>
          </Thead>
          <Tbody>
            {rows}
          </Tbody>
        </Table>
      </TableContainer></div>
    )
}

  const CategoryFormModal = () => {
    const { isOpen, onOpen, onClose } = useDisclosure()
    const [value, setValue] = React.useState('')
    const handleChange = (event) => setValue(event.target.value)
  
    const submitForm = async() => {
      fetch("http://localhost:3001/categories/create", {
        method: 'POST',
        headers: { 'Content-Type': 'application/json'},
        body: JSON.stringify({
          "name": value
        })
      })
        .then(res => console.log(res))
    }
  
    return (
      <>
        <div onClick={onOpen}>POST</div>
        <Modal isOpen={isOpen} onClose={onClose}>
          <ModalOverlay />
          <ModalContent>
            <ModalHeader>Create New Category</ModalHeader>
            <Input value={value} variant='flushed' placeholder='category name' onChange={handleChange}/>
            <ModalCloseButton />
            <ModalFooter>
              <Button colorScheme='blue' mr={3} onClick={submitForm}>
                Save
              </Button>
              <Button onClick={onClose}>Cancel</Button>
            </ModalFooter>
          </ModalContent>
        </Modal>
      </>
    )
  
  }

  const CategoryEditFormModal = (props) => {  
    const { isOpen, onOpen, onClose } = useDisclosure()
    const [name, setName] = React.useState()
    const handleNameChange = (event) => setName(event.target.value)
    const fetchMarkedCategory = async() => {        
        const res = await fetch("http://localhost:3001/categories/get/" + props.markedCategoryId)
        return res.json();
    }

    const {data: categoryData, status: categoryStatus} = useQuery('category', fetchMarkedCategory, {
        // onSuccess: () => {
        //     console.log("Get data!");
        //     console.log(categoryData.name);
        //     setName(categoryData.name);
        //   }
    })    

    const submitForm = async() => {
        fetch("http://localhost:3001/categories/update/" + props.markedCategoryId, {
            method: 'patch',
            mode: 'no-cors',
            headers: { 'Content-Type': 'application/json'},
            body: JSON.stringify({
                "name": name
            })
        }).then(res => console.log(res))
    }
    
    return (
      <>
        <Button onClick={onOpen}>UPDATE</Button>
        <Modal isOpen={isOpen} onClose={onClose}>
          <ModalOverlay />
          <ModalContent>
            {categoryStatus === 'loading' && <div>Loading...</div>}
            {categoryStatus === 'success' && <>
            <ModalHeader>Edit Marked Category</ModalHeader>
            <label>Name</label>
            <Input defaultValue={name} value={name} variant='flushed' placeholder={name} onChange={handleNameChange}/>
            <ModalCloseButton />
            <ModalFooter>
              <Button colorScheme='blue' mr={3} onClick={submitForm}>
                Save
              </Button>
              <Button onClick={onClose}>Cancel</Button>
            </ModalFooter></>}
          </ModalContent>
        </Modal>
      </>
    )  
  }
  
CategoryTable.prototype = {
    categoryList: PropTypes.array
}

export default CategoryTable;