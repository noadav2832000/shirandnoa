import { useQuery, useMutation, useQueryClient } from 'react-query';
import axios from 'axios';
import { IBrand } from '../../../interfaces/clothes';

const url = "http://localhost:3001/brands";

export const getById = (id: string) => {
  const queryUrl = url + "/" + id;
  return axios.get(queryUrl).
  then(
    response => response.data
    ).
  catch(error => {console.log(error)})
}
   
const fetch = () => {
    return axios.get(url).then(response => response.data).catch((error) => error)
}

export const useData = () => {
  return useQuery("brands", fetch, {
      onSuccess: (res) => res.data,
      onError: (error) => console.log(error)
  })
}

const remove = (id: string) => {
  const queryUrl = url + "/" + id;
  return axios.delete(queryUrl)
}
  
export const useDelete = () => {
  const queryClient = useQueryClient()
  return useMutation(remove, {
    onSuccess: () => {
      queryClient.invalidateQueries('brands')
    }
  })
}

const add = (newBrand: IBrand) => {
  return axios.post(url, newBrand)
}
  
export const useAdd = () => {
  const queryClient = useQueryClient()
  return useMutation(add, {
    onSuccess: () => {
      queryClient.invalidateQueries('brands')
    }
  })
}

const update = (newBrand: IBrand) => {
  const queryUrl = url + "/" + newBrand._id;
  return axios.patch(queryUrl, newBrand)
}

export const useUpdate = () => {
  const queryClient = useQueryClient()
  return useMutation(update, {
    onSuccess: () => {
      queryClient.invalidateQueries('brands')
    }
  })
}


// class BrandQueries implements IManagerQueries {  
//   url = "http://localhost:3001/brands";

//   getById = (id: string) => {
//     const queryUrl = this.url + "/get/" + id;
//     return axios.get(queryUrl).then(response => response.data).catch(error => {console.log(error)})
//   }

//   add = (newBrand: IBrand) => {
//       return axios.post(this.url, newBrand)
//   }

//   fetch = () => {
//       return axios.get(this.url).then(response => response.data).catch((error) => error)
//     }

//   useData() {
//       return useQuery("brands", this.fetch, {
//           onSuccess: (res) => res.data,
//           onError: (error) => console.log(error)
//       })
//     }

//   useAdd = () => {
//       return useMutation(this.add)
//   }

//   useDelete: (id: string) => Promise<object>;
  
//   useEdit: (id: string, newItem: any) => Promise<object>;

// }

// export default BrandQueries;