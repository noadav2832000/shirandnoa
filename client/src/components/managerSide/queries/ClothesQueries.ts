import { useQuery, useMutation, useQueryClient } from 'react-query';
import axios from 'axios';
import IClothes from '../../../interfaces/clothes';

const url = "http://localhost:3001/clothes";


export const getById = (id: string) => {
  const queryUrl = url + "/" + id;
  return axios.get(queryUrl).
  then(
    response => response.data
    ).
  catch(error => {console.log(error)})
}
   
const fetch = () => {
    return axios.get(url).then(response => response.data).catch((error) => error)
}

export const useData = () => {
  return useQuery("clothes", fetch, {
      onSuccess: (res) => res.data,
      onError: (error) => console.log(error)
  })
}

const remove = (id: string) => {
  const queryUrl = url + "/" + id;
  return axios.delete(queryUrl)
}
  
export const useDelete = () => {
  const queryClient = useQueryClient()
  return useMutation(remove, {
    onSuccess: () => {
      queryClient.invalidateQueries('clothes')
    }
  })
}

const add = (newCloth: IClothes) => {
  return axios.post(url, newCloth)
}
  
export const useAdd = () => {
  const queryClient = useQueryClient()
  return useMutation(add, {
    onSuccess: () => {
      queryClient.invalidateQueries('clothes')
    }
  })
}

const update = (newCloth: IClothes) => {
  const queryUrl = url + "/" + newCloth._id;
  return axios.patch(queryUrl, newCloth)
}

export const useUpdate = () => {
  const queryClient = useQueryClient()
  return useMutation(update, {
    onSuccess: () => {
      queryClient.invalidateQueries('clothes')
    }
  })
}
