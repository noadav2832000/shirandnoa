import { useQuery, useMutation } from 'react-query';
import axios from 'axios';
import {  ISex } from '../../../interfaces/clothes';
// import { IManagerQueries } from './index';

// export class SexQueries implements IManagerQueries {  
//   url = "http://localhost:3001/sexes";

//   getById = (id: string) => {
//     const queryUrl = this.url + "/get/" + id;
//     return axios.get(queryUrl).then(response => response.data).catch(error => {console.log(error)})
//   }

//   add = (newBrand: ISex) => {
//       return axios.post(this.url, newBrand)
//   }

//   fetch = () => {
//       return axios.get(this.url).then(response => response.data).catch((error) => error)
//     }

//   useData = () => {
//       return useQuery("sexes", this.fetch, {
//           onSuccess: (res) => res.data,
//           onError: (error) => console.log(error)
//       })
//     }

//   useAdd = () => {
//       return useMutation(this.add)
//   }

//   useDelete: (id: string) => Promise<object>;
  
//   useEdit: (id: string, newItem: any) => Promise<object>;

// }

const url = "http://localhost:3001/sexes";
 
export const getById = (id: string) => {
    const queryUrl = url + "/get/" + id;
    return axios.get(queryUrl).then(response => response.data).catch(error => {console.log(error)})
  }


const add = (newSex: ISex) => {
    return axios.post(url, newSex)
}

const fetch = () => {
    return axios.get(url).then(response => response.data).catch((error) => error)
  }

export const useData = () => {
    return useQuery("brands", fetch, {
        onSuccess: (res) => res.data,
        onError: (error) => console.log(error)
    })
  }

export const useAdd = () => {
    return useMutation(add)
}