import { UseQueryResult } from 'react-query';
import * as BrandQueries from './BrandsQueries';
import * as SexQueries from './SexQueries';
import * as ClothesQueries from './ClothesQueries';
import * as CategoryQueries from './CategoryQueries';

/*export interface IManagerQueries {
    getById: (id: string) => Promise<object>
    useData: () => UseQueryResult
    useAdd: (newItem: any) => any
    useDelete: (id: string) => Promise<object>
    useEdit: (id: string, newItem: any) => Promise<object>
}*/

export {BrandQueries, SexQueries, ClothesQueries, CategoryQueries}

// module.exports = {
//     BrandQueries: BrandQueries,
//     SexQueries: SexQueries,
//     ClothesQueries: ClothesQueries,
//     CategoryQueries: CategoryQueries
// }