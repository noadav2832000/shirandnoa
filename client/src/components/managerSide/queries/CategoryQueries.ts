import { useQuery, useMutation, useQueryClient } from 'react-query';
import axios from 'axios';
import {  ICategory } from '../../../interfaces/clothes';

const url = "http://localhost:3001/categories";

export const getById = (id: string) => {
  const queryUrl = url + "/" + id;
  return axios.get(queryUrl).
  then(
    response => response.data
    ).
  catch(error => {console.log(error)})
}
   
const fetch = () => {
    return axios.get(url).then(response => response.data).catch((error) => error)
}

export const useData = () => {
  return useQuery("categories", fetch, {
      onSuccess: (res) => res.data,
      onError: (error) => console.log(error)
  })
}

const remove = (id: string) => {
  const queryUrl = url + "/" + id;
  return axios.delete(queryUrl)
}
  
export const useDelete = () => {
  const queryClient = useQueryClient()
  return useMutation(remove, {
    onSuccess: () => {
      queryClient.invalidateQueries('categories')
    }
  })
}

const add = (newCategory: ICategory) => {
  return axios.post(url, newCategory)
}
  
export const useAdd = () => {
  const queryClient = useQueryClient()
  return useMutation(add, {
    onSuccess: () => {
      queryClient.invalidateQueries('categories')
    }
  })
}

const update = (newCategory: ICategory) => {
  const queryUrl = url + "/" + newCategory._id;
  return axios.patch(queryUrl, newCategory)
}

export const useUpdate = () => {
  const queryClient = useQueryClient()
  return useMutation(update, {
    onSuccess: () => {
      queryClient.invalidateQueries('categories')
    }
  })
}
