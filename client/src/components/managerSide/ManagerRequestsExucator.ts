export const enum ControlersTypes
  {Clothes = 'clothes', Brands = 'brands', Categories = 'categories'}
  

class ManagerRequestsExucator {
  url: string;

  constructor(controllerType: ControlersTypes) {
    this.url = "http://localhost:3001/" + controllerType;
  }

  getAll = async() => {
    return fetch(this.url)
    .then(res => res.json())
    .catch(error => {console.log(error)})
  }

  getById = async(id: String) => {
    const queryUrl = this.url + "/get/" + id
    return fetch(queryUrl)
    .then(res => res.json())
    .catch(error => {console.log(error)})
  }
  
  create = async(newData) => {
    const queryUrl = this.url + "/create"
    return fetch(queryUrl, 
      {
        method: "post",
        headers: { 'Content-Type': 'application/json'},
        body: JSON.stringify(newData)
      })
    .then(res => res.json())
    .catch(error => {console.log(error)})
  }

  update = async(id: String, newData) => {
    const queryUrl = this.url + "/update/" + id
    return fetch(queryUrl, {
      method: "patch",
      headers: { 'Content-Type': 'application/json'},
      body: JSON.stringify(newData)
  })
    .then(res => res.json())
    .catch(error => {console.log(error)})
  }

  delete = async(id: String) => {
    const queryUrl = this.url + "/delete/" + id
    return fetch(queryUrl, {
      method: "delete"
    })
    .then(res => res.json())
    .catch(error => {console.log(error)})
  }
}

// import axios from 'axios';

// class ManagerRequestsExucator {
//   static getAllClothes = (): any => {
//     return axios({
//       method: "GET",
//       url: "http://localhost:3001/clothes",
//     }).then(
//       (response: any) => {
//         return response.data.items;
//       },
//       (error) => {
//         console.log(error);
//       }
//     );
//   };

//   static getAllBrands = () => {
//     return axios.get("http://localhost:3001/brands").then((response: any) => {
//         return response.data.items;
//       },
//       (error) => {
//         console.log(error);
//       }
//     );
//   };

//   static getAllCategories = () => {
//     return axios({
//       method: "GET",
//       url: "http://localhost:3001/category",
//     }).then(
//       (response: any) => {
//         return response.data.items;
//       },
//       (error) => {
//         console.log(error);
//       }
//     );
//   };
// }

export default ManagerRequestsExucator;