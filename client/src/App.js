// client/src/App.js

import React from "react";
import "./App.css";
import { BrowserRouter as Router,Routes, Route } from 'react-router-dom';
import HomeCatalog from "./components/pages/HomeCatalog"
import Contact from "./components/pages/Contact"
import  AboutUs from "./components/pages/AboutUs"
import SearchedClothes from "./components/pages/SearchedClothes";
import WomanCatalog from "./components/pages/WomanCatalog";
import ManCatalog from "./components/pages/ManCatalog";
import ManageData from "./components/managerSide/ManageData";
import {QueryClient, QueryClientProvider} from "react-query"
import { ChakraProvider } from "@chakra-ui/react";

const queryClient = new QueryClient()

function App() {
  // const [data, setData] = React.useState(null);
  // React.useEffect(() => {
  //   fetch("/api")
  //       .then((res) => res.json())
  //       .then((data) => setData(data.message));
  // }, []);

  return (
  <ChakraProvider>
  <QueryClientProvider client={queryClient}>
  <Router>
    <div className="App">
      <Routes>
        <Route exact path='/' element={< HomeCatalog />}></Route>
        <Route exact path='/about' element={< AboutUs />}></Route>
        <Route exact path='/contact' element={< Contact />}></Route>
        <Route exact path='/woman' element={< WomanCatalog />}></Route>
        <Route exact path='/man' element={< ManCatalog />}></Route>
        <Route exact path='/managerPart' element={< ManageData />}></Route>
        {/* <Route exact path='/searchedClothes/' element={< searchedClothes />}></Route> */}
        {/* <Route exact path={"/searchedClothes"} element={< SearchedClothes searchedText={searchedText}/>} ></Route> */}
        <Route exact path={`/searchedClothes/:searchedText`} render={(props) => <SearchedClothes {...props}/>} element={< SearchedClothes/>} ></Route>
      </Routes>
    </div>
  </Router>
  </QueryClientProvider>
  </ChakraProvider>
  );
}

export default App;