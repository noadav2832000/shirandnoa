export interface IBrand {
    _id: string,
    name: string
}

export interface ISex {
    _id: string,
    name: string
}

export interface ICategory {
    _id: string,
    name: string
}

export interface IClothes {
    _id: string,
    title: string,
    description: string,
    imgSrc: string,
    brand: IBrand,
    sex: ISex,
    category: ICategory
}

export default IClothes;